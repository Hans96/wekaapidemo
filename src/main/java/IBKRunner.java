import weka.classifiers.lazy.IBk;
import weka.classifiers.trees.J48;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

import java.io.IOException;

public class IBKRunner {
    private final String modelFile = "testdata/IBKmodel.model";

    public static void main(String[] args) {
        IBKRunner runner = new IBKRunner();
        runner.start();
    }

    private void start() {
        String datafile = "testdata/javatestfile-noremovall.arff";
        String unknownFile = "testdata/javatestfilena.arff";
        try {
            IBk ibk = new IBk();
            Instances instances = loadArff(datafile);
            printInstances(instances);
            ibk.buildClassifier(instances);
            saveClassifier(ibk);
            //loads model
            IBk fromFile = loadClassifier();
            //Instances unknownInstances = loadArff(unknownFile);
            //classifyNewInstance(fromFile, unknownInstances);
            //set class index to last attribute
            makePrediction(instances, fromFile);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void classifyNewInstance(IBk ibk, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = ibk.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        System.out.println("\nNew, labeled = \n" + labeled);
    }

    private IBk loadClassifier() throws Exception {
        // deserialize model
        return (IBk) weka.core.SerializationHelper.read(modelFile);
    }

    private void saveClassifier(IBk ibk) throws Exception {
        //post 3.5.5
        // serialize model
        weka.core.SerializationHelper.write(modelFile, ibk);

        // serialize model pre 3.5.5
//        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(modelFile));
//        oos.writeObject(j48);
//        oos.flush();
//        oos.close();
    }

    private IBk buildClassifier(Instances instances) throws Exception {
        String[] options = new String[1];
        options[0] = "-K";            // number of neighbors
        IBk ibk = new IBk();         // new instance of ibk
        ibk.setOptions(options);     // set the options
        ibk.buildClassifier(instances);   // build classifier
        return ibk;
    }

    private void printInstances(Instances instances) {
        int numAttributes = instances.numAttributes();

        for (int i = 0; i < numAttributes; i++) {
            System.out.println("attribute " + i + " = " + instances.attribute(i));
        }


        System.out.println("class index = " + instances.classIndex());
//        Enumeration<Instance> instanceEnumeration = instances.enumerateInstances();
//        while (instanceEnumeration.hasMoreElements()) {
//            Instance instance = instanceEnumeration.nextElement();
//            System.out.println("instance " + instance. + " = " + instance);
//        }

        //or
        int numInstances = instances.numInstances();
        for (int i = 0; i < numInstances; i++) {
            if (i == 5) break;
            Instance instance = instances.instance(i);
            System.out.println("instance = " + instance);
        }
    }

    private Instances loadArff(String datafile) throws IOException {
        try {
            DataSource source = new DataSource(datafile);
            Instances data = source.getDataSet();
            // setting class attribute if the data format does not provide this information
            // For example, the XRFF format saves the class attribute information as well
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);
            return data;
        } catch (Exception e) {
            throw new IOException("could not read from file");
        }
    }

    private void makePrediction(Instances instances, IBk fromFile) throws Exception {
        for(int i = 0; i < instances.numInstances(); i++){
            Instance inst = instances.instance(i);
            double actualClassValue  = instances.instance(i).classValue();
            //will print class value
            String actual=instances.classAttribute().value((int)actualClassValue);
            double result = fromFile.classifyInstance(inst);
            //will print predicted value
            String prediction=instances.classAttribute().value((int)result );
            if (result != actualClassValue) {
                System.out.println("predicted status : " + prediction + ", actually : " + actual);
            }
        }
        System.out.println(instances.size());
        }
    }

